---
layout: master
title: Privacy
description: Privacy
---


# Privacy

This website is made with [GitLab Pages]("https://docs.gitlab.com/ee/user/project/pages/"). skinship (“we”, “our”) does not track your visit or activity on skinship.xyz (“the website”), and does not make use of any cookies. Third-party services - i.e. GitLab, Cloudflare and YouTube - may be tracking some statistics about your visit of the website.