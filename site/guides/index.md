---
layout: master
highlight: guides
description: skinning guides
title: skinship | Guides
---

# Guides

-   [Centring Accuracy on the Playfield](./centring_accuracy)
-   [Creating your Personal Mixed Skin](./mixing_skins)
-   [Creating Instant Fading HitCircles](./insta_fade_hc)
-   [Creating Non-Pulsating Combo Numbers](./non_pulsating_combo_numbers)
-   [Importing Skins into osu!](./installing_skins)
-   [Using Dots for HitCircle Numbers](./dots_as_hitcircle_numbers)
